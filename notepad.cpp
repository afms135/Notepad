#include "notepad.h"
#include "ui_notepad.h"

Notepad::Notepad(QWidget *parent):QMainWindow(parent),ui(new Ui::Notepad)
{
	ui->setupUi(this);
	newFile = true;
	setWindowTitle("Untitled - Notepad");
	setAcceptDrops(true);
	readsettings();

	finddialog = new Find(this);
	statustext = new QLabel("Ln 1, Col 1");
	ui->statusBar->addPermanentWidget(statustext,0);

	QStringList args = QApplication::arguments();
	if(args.count() == 2)
	{
		openfile(args.at(1));
		newFile = false;
		currentfile = args.at(1);
	}

	connect(ui->actionNew,SIGNAL(triggered()),this,SLOT(newf()));
	connect(ui->actionOpen,SIGNAL(triggered()),this,SLOT(open()));
	connect(ui->actionSave,SIGNAL(triggered()),this,SLOT(save()));
	connect(ui->actionSave_As,SIGNAL(triggered()),this,SLOT(saveas()));
	connect(ui->actionPage_Setup,SIGNAL(triggered()),this,SLOT(pagesetup()));
	connect(ui->actionPrint,SIGNAL(triggered()),this,SLOT(print()));
	connect(ui->actionExit,SIGNAL(triggered()),this,SLOT(close()));

	connect(ui->actionUndo,SIGNAL(triggered()),ui->plainTextEdit,SLOT(undo()));
	connect(ui->actionRedo,SIGNAL(triggered()),ui->plainTextEdit,SLOT(redo()));
	connect(ui->actionCut,SIGNAL(triggered()),ui->plainTextEdit,SLOT(cut()));
	connect(ui->actionCopy,SIGNAL(triggered()),ui->plainTextEdit,SLOT(copy()));
	connect(ui->actionPaste,SIGNAL(triggered()),ui->plainTextEdit,SLOT(paste()));
	connect(ui->actionDelete,SIGNAL(triggered()),this,SLOT(deletetext()));
	connect(ui->actionFind,SIGNAL(triggered()),this,SLOT(find()));
	connect(ui->actionFind_Next,SIGNAL(triggered()),this,SLOT(findnext()));
	connect(ui->actionReplace,SIGNAL(triggered()),this,SLOT(replace()));
	connect(ui->actionGoto,SIGNAL(triggered()),this,SLOT(go_to()));
	connect(ui->actionSelect_All,SIGNAL(triggered()),ui->plainTextEdit,SLOT(selectAll()));
	connect(ui->actionTime_Date,SIGNAL(triggered()),this,SLOT(timedate()));

	connect(ui->actionWord_Wrap,SIGNAL(triggered(bool)),this,SLOT(wordwrap(bool)));
	connect(ui->actionFont,SIGNAL(triggered()),this,SLOT(font()));
	connect(ui->actionStatus_Bar,SIGNAL(triggered(bool)),ui->statusBar,SLOT(setVisible(bool)));
	connect(ui->actionAbout,SIGNAL(triggered()),this,SLOT(about()));

	connect(ui->plainTextEdit,SIGNAL(undoAvailable(bool)),ui->actionUndo,SLOT(setEnabled(bool)));
	connect(ui->plainTextEdit,SIGNAL(redoAvailable(bool)),ui->actionRedo,SLOT(setEnabled(bool)));
	connect(ui->plainTextEdit,SIGNAL(copyAvailable(bool)),ui->actionCopy,SLOT(setEnabled(bool)));
	connect(ui->plainTextEdit,SIGNAL(copyAvailable(bool)),ui->actionCut,SLOT(setEnabled(bool)));
	connect(ui->plainTextEdit,SIGNAL(copyAvailable(bool)),ui->actionDelete,SLOT(setEnabled(bool)));
	connect(ui->plainTextEdit,SIGNAL(cursorPositionChanged()),this,SLOT(status()));
	connect(finddialog,SIGNAL(finished(int)),this,SLOT(findword(int)));
}

Notepad::~Notepad()
{
	delete ui;
}

void Notepad::newf()
{
	if (saved()==true)
	{
		ui->plainTextEdit->clear();
		this->setWindowTitle("Untitled - Notepad");
		newFile = true;
	}
}

void Notepad::open()
{
	if(saved()==true)
	{
		QString filename = QFileDialog::getOpenFileName(this, "Open File", "","Text Files (*.txt);;All files (*)");
		if (filename == NULL)
			return;
		openfile(filename);
		currentfile = filename;
		newFile = false;
	}
}

void Notepad::save()
{
	if(newFile == true)
		saveas();
	else
		savefile(currentfile);
}

void Notepad::saveas()
{
	QString filename = QFileDialog::getSaveFileName(this, "Save File", "","Text Files (*.txt);;All Files (*)");
	if (filename == NULL)
		return;
	savefile(filename);
	currentfile = filename;
	newFile = false;
}

void Notepad::pagesetup()
{
	QPageSetupDialog setup(&printer, this);
	setup.exec();
}

void Notepad::print()
{
	QPrintDialog printDialog(&printer,this);
	if (printDialog.exec() == QDialog::Accepted)
	{
		ui->plainTextEdit->print(&printer);
	}
}

void Notepad::deletetext()
{
	ui->plainTextEdit->textCursor().removeSelectedText();
}

void Notepad::find()
{
	finddialog->mode(false);
	finddialog->show();
}

void Notepad::findnext()
{
	if(finddialog->findtext().isEmpty())
		find();
	else
		findword(2);
}

void Notepad::replace()
{
	finddialog->mode(true);
	finddialog->show();
}

void Notepad::go_to()
{
	bool ok;
	int line = QInputDialog::getInt(this,"Goto","Line Number:",0,1,ui->plainTextEdit->document()->lineCount(),1,&ok);
	if(ok)
	{
		QTextCursor cur(ui->plainTextEdit->document()->findBlockByLineNumber(line-1));
		ui->plainTextEdit->setTextCursor(cur);
	}
}

void Notepad::timedate()
{
	ui->plainTextEdit->insertPlainText(QDateTime::currentDateTimeUtc().toString("hh:mm dd/MM/yyyy"));
}

void Notepad::wordwrap(bool b)
{
	if (b)
		ui->plainTextEdit->setWordWrapMode(QTextOption::WrapAtWordBoundaryOrAnywhere);
	else
		ui->plainTextEdit->setWordWrapMode(QTextOption::NoWrap);
}

void Notepad::font()
{
	QFont font = QFontDialog::getFont(0, ui->plainTextEdit->font());
	if(font.family()=="Comic Sans MS")
	{
		int val = QMessageBox::question(this,"Mentaly Ill?","Do you have brain damage?",QMessageBox::Yes,QMessageBox::No);
		if (val==QMessageBox::No)
			this->font();
	}
	ui->plainTextEdit->setFont(font);
}

void Notepad::about()
{
	QPixmap *icon = new QPixmap(":/about.png");
	QMessageBox about;
	about.setText(tr("A Notepad Application.\n""Version 2.0\n""By Alexander Strachan"));
	about.setWindowTitle(tr("About Notepad"));
	about.setIconPixmap(*icon);
	about.exec();
}

void Notepad::setTitle(QString path)
{
	QFileInfo file(path);
	QString title = file.fileName();
	title.append(" - Notepad");
	setWindowTitle(title);
}

void Notepad::status()
{
	QTextCursor current = ui->plainTextEdit->textCursor();
	QString status = QString("Ln %1, Col %2").arg(current.blockNumber()+1).arg(current.positionInBlock()+1);
	statustext->setText(status);
}

void Notepad::openfile(QString filepath)
{
	if(filepath=="")
		return;

	QFile file(filepath);
	if (!file.open(QFile::ReadOnly | QFile::Text))
	{
		QMessageBox::critical(this,"Open Error","Cannot Open File");
	}
	else
	{
		QTextStream in(&file);
		ui->plainTextEdit->setPlainText(in.readAll());
		setTitle(filepath);
		file.close();
	}
}

void Notepad::savefile(QString filepath)
{
	if(filepath=="")
		return;

	QFile file(filepath);
	if (!file.open(QFile::WriteOnly | QFile::Text))
	{
		QMessageBox::critical(this,"Save Error","Cannot Save File");
	}
	else
	{
		QTextStream out(&file);
		out << ui->plainTextEdit->toPlainText();
		setTitle(filepath);
		file.close();
		ui->plainTextEdit->document()->setModified(false);
	}
}

bool Notepad::saved()
{
	if (ui->plainTextEdit->document()->isModified()==true)
	{
		int val = QMessageBox::question(this,"Unsaved Changes","Do you want to save your changes?",QMessageBox::Save,QMessageBox::Discard,QMessageBox::Cancel);
		switch (val)
		{
		case QMessageBox::Save:
			save();
			return true;
			break;
		case QMessageBox::Discard:
			return true;
			break;
		case QMessageBox::Cancel:
			return false;
			break;
		}
	}
	return true;
}

void Notepad::findword(int i)
{
	if(i==2)//find
	{
		if(ui->plainTextEdit->find(finddialog->findtext(),finddialog->flags)==false)
			QMessageBox::information(this,"Notepad","Cannot find \""+finddialog->findtext()+"\"");
	}
	else if(i==3)//replace
	{
		if(ui->plainTextEdit->find(finddialog->findtext(),finddialog->flags)==false)
			QMessageBox::information(this,"Notepad","Cannot find \""+finddialog->findtext()+"\"");
		else
			ui->plainTextEdit->textCursor().insertText(finddialog->replacetext());
	}
	else if(i==4)//replace all
	{
		QTextCursor c;
		c.setPosition(0);
		ui->plainTextEdit->setTextCursor(c);
		while(ui->plainTextEdit->find(finddialog->findtext(),finddialog->flags))
		{
			ui->plainTextEdit->textCursor().insertText(finddialog->replacetext());
		}
	}
}

void Notepad::dragEnterEvent(QDragEnterEvent *event)
{
	event->acceptProposedAction();
}

void Notepad::dropEvent(QDropEvent *event)
{
	if(saved()==true)
	{
		openfile(event->mimeData()->urls().at(0).toLocalFile());
		currentfile = event->mimeData()->urls().at(0).toLocalFile();
		newFile = false;
	}
}

void Notepad::closeEvent(QCloseEvent *event)
{
	if (saved() == true)
	{
		writesettings();
		event->accept();
	}
	else
		event->ignore();
}

void Notepad::readsettings()
{
	QSettings settings("afms135", "Notepad");
	restoreGeometry(settings.value("Geometry").toByteArray());
	QFont font;
	font.fromString(settings.value("Font").toString());
	ui->plainTextEdit->setFont(font);
	ui->actionWord_Wrap->setChecked(settings.value("Word Wrap",true).toBool());
	ui->actionStatus_Bar->setChecked(settings.value("Status Bar",true).toBool());
	wordwrap(settings.value("Word Wrap",true).toBool());
	ui->statusBar->setVisible(settings.value("Status Bar",true).toBool());
}

void Notepad::writesettings()
{
	QSettings settings("afms135", "Notepad");
	settings.setValue("Geometry",this->saveGeometry());
	settings.setValue("Font",ui->plainTextEdit->font().toString());
	settings.setValue("Word Wrap",ui->actionWord_Wrap->isChecked());
	settings.setValue("Status Bar",ui->actionStatus_Bar->isChecked());
}
