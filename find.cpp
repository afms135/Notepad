#include "find.h"
#include "ui_find.h"

Find::Find(QWidget *parent) : QDialog(parent),ui(new Ui::Find)
{
	ui->setupUi(this);

	connect(ui->Button_Find,SIGNAL(clicked()),this,SLOT(find()));
	connect(ui->Button_Replace,SIGNAL(clicked()),this,SLOT(replace()));
	connect(ui->Button_ReplaceAll,SIGNAL(clicked()),this,SLOT(replaceall()));
	connect(ui->Button_Cancel,SIGNAL(clicked()),this,SLOT(close()));

	connect(ui->check_case,SIGNAL(toggled(bool)),this,SLOT(matchcase(bool)));
	connect(ui->check_word,SIGNAL(toggled(bool)),this,SLOT(matchword(bool)));
	connect(ui->radio_Up,SIGNAL(toggled(bool)),this,SLOT(direction(bool)));
}

Find::~Find()
{
	delete ui;
}

void Find::find()
{
	emit finished(2);
}

void Find::replace()
{
	emit finished(3);
}

void Find::replaceall()
{
	emit finished(4);
}

QString Find::findtext()
{
	return ui->lineFind->text();
}

QString Find::replacetext()
{
	return ui->lineReplace->text();
}

void Find::matchcase(bool b)
{
	if (b==true)
		flags |= QTextDocument::FindCaseSensitively;
	else
		flags &= ~QTextDocument::FindCaseSensitively;
}

void Find::matchword(bool b)
{
	if (b==true)
		flags |= QTextDocument::FindWholeWords;
	else
		flags &= ~QTextDocument::FindWholeWords;
}

void Find::direction(bool b)
{
	if (b==true)
		flags |= QTextDocument::FindBackward;
	else
		flags &= ~QTextDocument::FindBackward;
}

void Find::mode(bool mode) //TRUE is Replace FALSE is Find
{
	ui->Button_Replace->setVisible(mode);
	ui->Button_ReplaceAll->setVisible(mode);
	ui->lineReplace->setVisible(mode);
	ui->label_2->setVisible(mode);
	ui->groupBox->setVisible(!mode);
	if (mode==true)
	{
		this->setWindowTitle("Replace");
		this->setFixedSize(314,132);
	}
	else
	{
		this->setWindowTitle("Find");
		this->setFixedSize(314,104);
	}
}


