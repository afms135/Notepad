#ifndef NOTEPAD_H
#define NOTEPAD_H

#include <QMainWindow>
#include <QMessageBox>
#include <QtPrintSupport>
#include <QLabel>
#include <QDateTime>
#include <QFontDialog>
#include <QSettings>
#include "find.h"

namespace Ui
{
	class Notepad;
}

class Notepad : public QMainWindow
{
	Q_OBJECT
	
public:
	explicit Notepad(QWidget *parent = 0);
	~Notepad();
	
protected:
	void closeEvent(QCloseEvent *event);
	void dropEvent(QDropEvent *event);
	void dragEnterEvent(QDragEnterEvent *event);

private:
	Ui::Notepad *ui;
	QString currentfile;
	QLabel *statustext;
	QPrinter printer;
	Find *finddialog;
	bool newFile;
	void setTitle(QString path);
	bool saved();
	void openfile(QString filepath);
	void savefile(QString filepath);
	void readsettings();
	void writesettings();

private slots:
	//File
	void newf();
	void open();
	void save();
	void saveas();
	void pagesetup();
	void print();
	//Edit
	void deletetext();
	void find();
	void findnext();
	void replace();
	void go_to();
	void timedate();
	//Format
	void wordwrap(bool b);
	void font();
	//Help
	void about();

	//Other
	void status();
	void findword(int i);
};

#endif // NOTEPAD_H
