#ifndef FIND_H
#define FIND_H

#include <QDialog>
#include <QTextDocument>

namespace Ui
{
	class Find;
}

class Find : public QDialog
{
	Q_OBJECT
	
public:
	explicit Find(QWidget *parent = 0);
	~Find();
	void mode(bool mode);
	QString findtext();
	QString replacetext();
	QTextDocument::FindFlags flags;

private:
	Ui::Find *ui;

private slots:
	void find();
	void replace();
	void replaceall();
	void matchcase(bool b);
	void matchword(bool b);
	void direction(bool b);
};

#endif // FIND_H
