#-------------------------------------------------
#
# Project created by QtCreator 2013-05-19T16:14:42
#
#-------------------------------------------------

QT       += core gui widgets printsupport

TARGET = Notepad
TEMPLATE = app

SOURCES += main.cpp\
        notepad.cpp \
    find.cpp

HEADERS  += notepad.h \
    find.h

FORMS    += notepad.ui \
    find.ui

RESOURCES += \
    icon.qrc

win32:RC_FILE = icon.rc
